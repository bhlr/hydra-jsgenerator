<?php
/*
 * We return a script made for any JSON-LD API. This contain all methods
 * to each operations in the J-LD defined by te API.
 * @author Alexander Baquiax (alexander_ges@galileo.edu)
 * @creation-data May-22-2013
 * @vendor Galileo Educational System (www.galileo.edu)
 */

require_once("../hydra-client/vendor/autoload.php");
require_once("JSGenerator.php");
use ML\HydraClient\Client as HydraClient;
use ML\JsonLD\JsonLD;
header("Content-type: text/javascript");
$entry_point = (!empty($_REQUEST["url"])) ? $_REQUEST["url"] : "http://10.0.2.186:9000/mindmeister/api";
$api_name = (!empty($_REQUEST["namespace"])) ? $_REQUEST["namespace"] : "jsonLdAPI";
$method = (!empty($_REQUEST["method"])) ? $_REQUEST["method"] : "GET";
?>
/*
 * JSON-LD meta-data.
 * Entry-point: <?php echo $entry_point ?>
 *
 */

(function (<?php echo $api_name; ?>, $, undefined) {
    // Common function to initialize XML Http Request object 
    function getHttpRequestObject() {
        // Define and initialize as false
        var xmlHttpRequst = false;
      
        // Mozilla/Safari/Non-IE
	if (window.XMLHttpRequest) {
            xmlHttpRequst = new XMLHttpRequest();
	}
	// IE
	else if (window.ActiveXObject) {
            xmlHttpRequst = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlHttpRequst;
    }

    // Does the AJAX call to URL specific with rest of the parameters
    function doAjax(url, method, responseHandler, data) {
        // Set the variables
        url = url || "";
	method = method || "GET";
	async = true;
	data = data || null;
    
	if(url == "") {
	    alert("URL can not be null/blank");
	    return false;
	}
	
	var xmlHttpRequst = getHttpRequestObject();
    
	// If AJAX supported
	if(xmlHttpRequst != false) {
	    // Open Http Request connection
	    if(method == "GET") {
	        url = url + "?" + data;
		data = null;
	    }
	    xmlHttpRequst.open(method, url, async);
	    
	    // Set request header (optional if GET method is used)
	    if(method == "POST") {
	        xmlHttpRequst.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    }
	    
	    // Assign (or define) response-handler/callback when ReadyState is changed.
	    xmlHttpRequst.onreadystatechange = responseHandler;
	    // Send data
	    xmlHttpRequst.send(data);
	} else {
	    alert("Please use browser with Ajax support.!");
	}
    }
    
<?php
    $jsGenerator = new JSGenerator($entry_point,$method);
    $entry_doc = JsonLD::expand($jsGenerator->get_contents($entry_point,$method),(object)array("base" => $jsGenerator->getServerPath()));
    echo $jsGenerator->makeJSFunction($api_name,$entry_doc, true);
?>
}) (window.<?php echo $api_name;?> = window.<?php echo $api_name;?> || {});